<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Log In</title>
        <base href="<?= $web_root ?>"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <div class="container-fluid">
            <nav  class="navbar navbar-dark bg-primary navbar-expand-lg ">
                <div class="container-fluid">
                    <a href="#" class="navbar-brand">Stuck Overflow</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item ">
                                <a href="post/ask" class="nav-link">Ask a question</a>
                            </li>
                            <li class="nav-item ">
                                <a href="#" class="nav-link">Questions</a>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="#" class="nav-link"><i  class="fa fa-user fa-fw"></i>  <?php echo $user->UserName; ?></a>
                                <?php else: ?>
                                    <a href="user/signup" class="nav-link"><i  class="fa fa-user fa-fw"></i></a>
                                <?php endif; ?>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="setup/export" class="nav-link"> <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                <?php else: ?>
                                    <a href="user/login" class="nav-link"><i class="fa fa-sign-in" aria-hidden="true" ></i></a>
                                <?php endif; ?>


                            </li>
                        </ul>
                    </div>
                </div>

            </nav>
        </div>
        
        <div class="container-fluid">


            <form action="post/edit" method="post">

                <div class="form-group">
                    <h5>Body</h5>
                    <p class="text-sm-left font-weight-lighter">Include all the information someone would need to answer your question</p>
                    <input type="hidden" name="idpost"   value="<?= $idpost ?>"/>
                    <input type="hidden" name="idanwer"   value="<?= $idanwer ?>"/>
                    <div class="input-group input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-pencil"></i></span>
                        </div>
                        <textarea class="form-control"name="body"  placeholder="Message" rows="10" value="<?= $body ?>" required="required" ></textarea>
                    </div>
                </div>
                <input type="submit" value="Update Your Anwer"   class="btn btn-outline-info btn-block btn-lg">
            </form>


            <?php if (count($errors) != 0): ?>
                <div class='errors'>
                    <p>Please correct the following error(s) :</p>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li><?= $error ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>

    </body>
</html>



