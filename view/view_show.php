<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Log In</title>
        <base href="<?= $web_root ?>"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container-fluid">
            <nav  class="navbar navbar-dark bg-primary navbar-expand-lg ">
                <div class="container-fluid">
                    <a href="#" class="navbar-brand">Stuck Overflow</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item ">
                                <a href="post/ask" class="nav-link">Ask a question</a>
                            </li>
                            <li class="nav-item ">
                                <a href="#" class="nav-link">Questions</a>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="#" class="nav-link"><i  class="fa fa-user fa-fw"></i>  <?php echo $user->UserName; ?></a>
                                <?php else: ?>
                                    <a href="user/signup" class="nav-link"><i  class="fa fa-user fa-fw" aria-hidden="true"></i></a>
                                <?php endif; ?>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="setup/export" class="nav-link"> <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                <?php else: ?>
                                    <a href="user/login" class="nav-link"><i class="fa fa-sign-in" aria-hidden="true" ></i></a>
                                <?php endif; ?>


                            </li>


                        </ul>
                    </div>
                </div>

            </nav>
        </div>
        <div class="container">

            <?php foreach ($datas as $data): ?>

                <div id='typeofquestion'>
                    <h5><?= $data->post->Title ?></h5>
                    <p>Asked 1 minute ago by <a href="#" ><?= $data->author->UserName ?></a>&nbsp;&nbsp;
                        <?php if ($user): ?>
                            <?php if ($data->author->UserName == $user->UserName): ?>
                                <a href='post/edit/<?= $data->post->PostId ?>/-1' ><span><i href='post/edit/<?= $data->post->PostId ?>'  class="fa fa-edit" aria-hidden="true" ></i></span></a>&nbsp;&nbsp;
                                <?php if ($data->sumAnwers <= 0): ?>

                                    <a href='post/delete/<?= $data->post->PostId ?>/-1/0' ><span><i class="fa fa-trash" aria-hidden="true" ></i></span></a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>


                    </p>
                </div>

                <div class="row no-gutters bg-light position-relative" id='question'>
                    <div class="col-md-2 mb-md-0 p-md-4">
                        <p><a  href='post/plus/<?= $data->post->PostId ?>' ><i class="fa fa-thumbs-up"  aria-hidden="true" ></i></a></p>
                        <?php if ($data->vote): ?>
                            <p><?= $data->vote ?></p>
                        <?php else: ?>
                            <p>0</p>
                        <?php endif; ?>

                        <p>votes</p><br/>
                        <a  href='post/minus/<?= $data->post->PostId ?>'  ><span><i  class="fa fa-thumbs-down"  aria-hidden="true" ></i></span></a>

                    </div>
                    <div class="col-md-10 position-static p-4 pl-md-0">
                        <h5 class="mt-0"><?= $data->post->Title ?></h5>
                        <p><?= $data->post->Body ?></p>

                    </div>
                </div>


                <?php if ($data->sumAnwers > 0): ?>
                    <div >
                        <p><?= $data->sumAnwers ?> anwer(s)</p>
                    </div>
                    <?php foreach ($data->anwers as $post): ?>
                        <div class="row no-gutters bg-light position-relative" id='answer'>
                            <div class="col-md-2 mb-md-0 p-md-4">
                                <p><a  href='post/plus/<?= $data->post->PostId ?>/<?= $post->PostId ?>' ><i  class="fa fa-thumbs-up" aria-hidden="true"  ></i></a></p>
                                <?php if ($post->Vote): ?>
                                    <p><?= $post->Vote ?></p>
                                <?php else: ?>
                                    <p>0</p>
                                <?php endif; ?>

                                <p>votes</p>
                                <p><a  href='post/minus/<?= $data->post->PostId ?>/<?= $post->PostId ?>' ><span><i   class="fa fa-thumbs-down"  aria-hidden="true" ></i></span></a></p>

                                <?php if ($user): ?>
                                    <?php if ($data->author->UserName == $user->UserName): ?>
                                        <p>
                                            <a href='post/update/<?= $data->post->PostId ?>/<?= $post->PostId ?>/1' ><span><i  class="fa fa-check"  aria-hidden="true"  ></i></span></a> 
                                            <a href='post/update/<?= $data->post->PostId ?>/<?= $post->PostId ?>/0' ><span> <i   class="fa fa-times"  aria-hidden="true" ></i></span></a>
                                        </p>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-10 position-static p-4 pl-md-0">
                                <h5 class="mt-0">Columns with stretched link</h5>
                                <p><?= $post->Body ?></p>
                                <p>Answered by just now by <a href="#" ><?= $post->Author->UserName ?></a>&nbsp;&nbsp;

                                    <?php if ($user): ?>
                                        <?php if ($post->Author->UserName == $user->UserName): ?>
                                            <a href='post/edit/<?= $data->post->PostId ?>/<?= $post->PostId ?>' ><span><i class="fa fa-edit" aria-hidden="true" ></i></span></a>&nbsp;&nbsp;
                                            <a href='post/delete/<?= $data->post->PostId ?>/<?= $post->PostId ?>/0' ><span><i class="fa fa-trash" aria-hidden="true" ></i></span></a>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>

                <?php else: ?>
                    <a href="user/signup" class="nav-link"><i  class="fa fa-user fa-fw" aria-hidden="true" ></i></a>
                <?php endif; ?>

            <?php endforeach; ?>
            <div class="container-fluid">


                <form action="post/writeAnwer" method="post">

                    <div class="form-group">
                        <h5>Your answer(s)</h5>
                        <input type="hidden" name="id"   value="<?= $data->post->PostId ?>"/>
                        <div class="input-group input-group-lg">

                            <textarea class="form-control" name="body" value="<?= $body ?>" placeholder="Put your anwer here" rows="10" required="required"></textarea>
                        </div>
                    </div>
                    <input type="submit" value="Post Your Anwer" class="btn btn-outline-info btn-block btn-lg">
                </form>


                <?php if (count($errors) != 0): ?>
                    <div class='errors'>
                        <p>Please correct the following error(s) :</p>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>

        </div>

    </body>
</html>



