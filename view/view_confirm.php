<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Log In</title>
        <base href="<?= $web_root ?>"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container-fluid">
            <nav  class="navbar navbar-dark bg-primary navbar-expand-lg ">
                <div class="container-fluid">
                    <a href="#" class="navbar-brand">Stuck Overflow</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item ">
                                <a href="post/ask" class="nav-link">Ask a question</a>
                            </li>
                            <li class="nav-item ">
                                <a href="#" class="nav-link">Questions</a>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="#" class="nav-link"><i  class="fa fa-user fa-fw"></i>  <?php echo $user->UserName; ?></a>
                                <?php else: ?>
                                    <a href="user/signup" class="nav-link"><i  class="fa fa-user fa-fw" aria-hidden="true"></i></a>
                                <?php endif; ?>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="setup/export" class="nav-link"> <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                <?php else: ?>
                                    <a href="user/login" class="nav-link"><i class="fa fa-sign-in" aria-hidden="true" ></i></a>
                                <?php endif; ?>


                            </li>


                        </ul>
                    </div>
                </div>

            </nav>
        </div>
        <div class=" d-flex justify-content-center mt-5">


            <div class="card text-center border-dark ">
                <div class="card-header">
                    <img  src="upload/icons8-delete-bin-64.png" alt="Card image cap">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure?</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Do  you really want to delete this post?</h5>
                    <p class="card-text">This process cannot be undone.</p>
                    
                </div>
                <div class="card-footer text-muted">
                    <a href='post/delete/<?= $idpost ?>/-2/1' class="card-link text-secondary">Cancel</a>
                    <a href='post/delete/<?= $idpost ?>/<?= $idanwer ?>/1' class="card-link text-danger">Delete</a>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="text-center">
                                <img src="upload/icons8-delete-bin-64.png"> 
                                <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure?</h5>
                            </div>   

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Do  you really want to delete this post?
                            This process cannot be undone.
                        </div>
                        <div class="modal-footer text-center">
                            <a href='post/delete/<?= $idpost ?>/-2/1' class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                            <a href='post/delete/<?= $idpost ?>/<?= $idanwer ?>/1' class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>



        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    </body>
</html>



