<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sign Up</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
             <div class="container-fluid">
            <nav  class="navbar navbar-dark bg-primary navbar-expand-lg ">
                <div class="container-fluid">
                    <a href="#" class="navbar-brand">Stuck Overflow</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item ">
                                <a href="post/ask" class="nav-link">Ask a question</a>
                            </li>
                            <li class="nav-item ">
                                <a href="#" class="nav-link">Questions</a>

                            </li>
                            <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="#" class="nav-link"><i  class="fa fa-user fa-fw"></i>  <?php echo $user->UserName; ?></a>
                                <?php else: ?>
                                    <a href="user/signup" class="nav-link"><i  class="fa fa-user fa-fw"></i></a>
                                <?php endif; ?>
                               
                            </li>
                           <li class="nav-item ">
                                <?php if ($user): ?>
                                    <a href="setup/export" class="nav-link"> <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                <?php else: ?>
                                    <a href="user/login" class="nav-link"><i class="fa fa-sign-in" aria-hidden="true" ></i></a>
                                <?php endif; ?>


                            </li>
                        </ul>
                    </div>
                </div>

            </nav>
        </div>
        <div class="signup-page">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-lg-6 offset-sm-1 offset-lg-3">				
                        <div class="form-bg">

                            <h2>Sign up </h2>
                            <form action="user/signup" method="post">


                                <div class="dropdown-divider"></div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" ><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" name="username" class="form-control" id="validationDefaultUsername" placeholder="Username" value="<?= $username ?>" aria-describedby="inputGroupPrepend2" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" ><i class="fa fa-lock"></i></span>
                                        </div>
                                        <input type="password"  name="password" class="form-control" value="<?= $password ?>" placeholder="Password" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" ><i class="fa fa-lock"></i></span>
                                        </div>
                                        <input type="password" name="password_confirm" class="form-control" value="<?= $password_confirm ?>" placeholder="Confirm Password" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" ><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" name="fullname" class="form-control" value="<?= $fullname ?>" placeholder="Fullname" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" ><i class="fa fa-at"></i></span>
                                        </div>
                                        <input type="email" name="email" class="form-control" value="<?= $email ?>" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Signup</button>
                                </div>
                            </form>
                            <?php if (count($errors) != 0): ?>
                                <div class='errors'>
                                    <br><br><p>Please correct the following error(s) :</p>
                                    <ul>
                                        <?php foreach ($errors as $error): ?>
                                            <li><?= $error ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

