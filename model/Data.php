<?php

require_once 'User.php';
require_once 'framework/Model.php';
require_once 'Vote.php';
require_once 'Post.php';

class Data {
    
    public $post;
    public $author;
    public $anwers = [];
    public $sumAnwers;
    public $vote;
    
    public function __construct($post, $author, $anwers,$sumAnwers, $vote) {

        $this->post = $post;
        $this->author = $author;
        $this->anwers = $anwers;
        $this->sumAnwers = $sumAnwers;
        $this->vote = $vote;
       
    }
    
    
    public static function getData() {
        $posts = Post::newest();
        $results = [];
        
         foreach ($posts as $row) {
             
             $author = User::get_user_by_id($row->Author);
             $anwers = Post::getResponses($row->PostId);
             $sumAnwers = count($anwers);
             $vote = Vote::getPostScore($row->PostId);
             
             $results[] = new Data($row,$author,$anwers,$sumAnwers,$vote);
        }
        return $results;
    }
    
     public static function getPostIdData($PostId) {
        $posts = Post::getPostbyId($PostId);
        $results = [];
        
         foreach ($posts as $row) {
             
             $author = User::get_user_by_id($row->Author);
             $anwers = Post::getResponses($row->PostId);
             $sumAnwers = count($anwers);
             $vote = Vote::getPostScore($row->PostId);
         
            $results[] = new Data($row,$author,$anwers,$sumAnwers,$vote);
        }
        return $results;
    }
    
     public static function searchData($search) {
        $posts = Post::search($search);
        $results = [];
        
         foreach ($posts as $row) {
             
             $author = User::get_user_by_id($row->Author);
             $anwers = Post::getResponses($row->PostId);
             $sumAnwers = count($anwers);
             $vote = Vote::getPostScore($row->PostId);
         
            $results[] = new Data($row,$author,$anwers,$sumAnwers,$vote);
        }
        return $results;
    }
    
     public static function unansweredData() {
        $posts = Post::unanswered();
        $results = [];
        
         foreach ($posts as $row) {
             
             $author = User::get_user_by_id($row->Author);
             $anwers = Post::getResponses($row->PostId);
             $sumAnwers = count($anwers);
             $vote = Vote::getPostScore($row->PostId);
          
             
            $results[] = new Data($row,$author,$anwers,$sumAnwers,$vote);
        }
        return $results;
    }
    
     public static function votesData() {
        $posts = Post::votes();
        $results = [];
        
         foreach ($posts as $row) {
             
             $author = User::get_user_by_id($row->Author);
             $anwers = Post::getResponses($row->PostId);
             $sumAnwers = count($anwers);
             $vote = Vote::getPostScore($row->PostId);
             
             $results[] = new Data($row,$author,$anwers,$sumAnwers,$vote);
        }
        return $results;
    }
    
    
}

