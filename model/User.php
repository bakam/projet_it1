<?php

require_once 'framework/Model.php';

class User extends Model
{

    public $UserId;
    public $UserName;
    public $Password;
    public $FullName;
    public $Email;

    public function __construct($UserName, $Password, $FullName, $Email, $UserId = NULL)
    {

        $this->UserId = $UserId;
        $this->UserName = $UserName;
        $this->Password = $Password;
        $this->FullName = $FullName;
        $this->Email = $Email;
    }

     public function write_post($post) {
        return $post->update();
    }
    
     public function delete_post($post) {
        return $post->delete($this);
    }
    
    public function get_posts() {
        return Post::get_posts($this);
    }
    
     public function get_other_users_and_relationships() {
        $query = self::execute("SELECT UserName,
                     (SELECT count(*) 
                      FROM vote 
                      WHERE UserId=:user and PostId=Post.PostId) as UserId,
                     (SELECT count(*) 
                      FROM vote
                      WHERE PostId=:user and UserId=User.UserId) as PostId
              FROM User 
              WHERE UserName <> :user 
              ORDER BY UserName ASC", array("user" => $this->UserName));
        return $query->fetchAll();
    }
    
   

    private static function add_vote($userid, $postid, $updown) {
        self::execute("INSERT INTO Vote VALUES (:userid,:postid,:updown)", array("userid"=>$userid, "postid"=>$postid, "updown"=>$updown));
        return true;
    }

    private static function delete_vote($userid, $postid) {
        self::execute("DELETE FROM vote WHERE UserId = :user AND PostId = :other", array("user"=>$userid, "other"=>$postid));
        return true;
    }
    
     public function update() {
        
        if(self::get_user_by_id($this->UserId))
            self::execute("UPDATE User SET UserName=:username,Password=:password, FullName=:fullname, Email=:email WHERE UserId=:userid ", 
                          array("username"=>$this->UserName, "password"=>$this->Password, "fullname"=>$this->FullName, "email"=>$this->Email));
        else
            self::execute("INSERT INTO User(UserName,Password,FullName,Email) VALUES(:username,:password,:fullname,:email)", 
                          array("username"=>$this->UserName, "password"=>$this->Password, "fullname"=>$this->FullName, "email"=>$this->Email));
        return $this;
    }

    public static function get_user_by_id($user) {
        
     
              
        $query = self::execute("SELECT * FROM User where UserId = :userid", array("userid"=>$user->UserId));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new User($data["UserName"], $data["Password"], $data["FullName"], $data["Email"], $data["UserId"]);
        }
    }

    public static function get_users() {
        $query = self::execute("SELECT * FROM user ", array());
        print_r("get_users");
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            print_r($row["UserName"]);
            $results[] = new User($row["UserName"], $row["Password"], $row["FullName"], $row["Email"], $row["UserId"]);
        }
        return $results;
    }
    
    private static function validate_password($password){
        $errors = [];
        if (strlen($password) < 8 || strlen($password) > 16) {
            $errors[] = "Password length must be between 8 and 16.";
        } if (!((preg_match("/[A-Z]/", $password)) && preg_match("/\d/", $password) && preg_match("/['\";:,.\/?\\-]/", $password))) {
            $errors[] = "Password must contain one uppercase letter, one number and one punctuation mark.";
        }
        return $errors;
    }
    
    public static function validate_passwords($password, $password_confirm){
        $errors = User::validate_password($password);
        if ($password != $password_confirm) {
            $errors[] = "You have to enter twice the same password.";
        }
        return $errors;
    }
    
    public static function validate_unicity($username){
        $errors = [];
        $user = self::get_User_by_UserName($username);
        if ($user) {
            $errors[] = "This user already exists.";
        } 
        return $errors;
    }

    //indique si un mot de passe correspond à son hash
    private static function check_password($clear_password, $hash) {
        return $hash === Tools::my_hash($clear_password);
    }

    //renvoie un tableau d'erreur(s) 
    //le tableau est vide s'il n'y a pas d'erreur.
    //ne s'occupe que de la validation "métier" des champs obligatoires (le pseudo)
    //les autres champs (mot de passe, description et image) sont gérés par d'autres
    //méthodes.
    public function validate(){
        $errors = array();
        if (!(isset($this->UserName) && is_string($this->UserName) && strlen($this->UserName) > 0)) {
            $errors[] = "UserName is required.";
        } if (!(isset($this->UserName) && is_string($this->UserName) && strlen($this->UserName) >= 3 && strlen($this->UserName) <= 16)) {
            $errors[] = "UserName length must be between 3 and 16.";
        } if (!(isset($this->UserName) && is_string($this->UserName) && preg_match("/^[a-zA-Z][a-zA-Z0-9]*$/", $this->UserName))) {
            $errors[] = "UserName must start by a letter and must contain only letters and numbers.";
        }
        if (!(isset($this->FullName) && is_string($this->FullName) && strlen($this->FullName) > 0)) {
            $errors[] = "FullName is required.";
        } if (!(isset($this->FullName) && is_string($this->FullName) && strlen($this->FullName) >= 3 && strlen($this->FullName) <= 16)) {
            $errors[] = "FullName length must be between 3 and 16.";
        } if (!(isset($this->FullName) && is_string($this->FullName) && preg_match("/^[a-zA-Z][a-zA-Z0-9]*$/", $this->FullName))) {
            $errors[] = "FullName must start by a letter and must contain only letters and numbers.";
        }
        if (!filter_var($this->Email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "email est mauvaise.";
        }
        return $errors;
    }
    
    
    public static function get_User_by_UserName($UserName)
    {
        
        $query = self::execute("SELECT * FROM user where UserName = :UserName", array("UserName" => $UserName));
        
        
        $data = $query->fetch();
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new User($data["UserName"], $data["Password"], $data["FullName"], $data["Email"], $data["UserId"]);
        }
    }

    public static function validate_login($UserName, $Password)
    {
        $errors = [];
        
        $User = User::get_User_by_UserName($UserName);
       
        if ($User) {
            if (!self::check_password($Password, $User->Password)) {
                $errors[] = "mauvais password. essaie encore.";
            }
        } else {
            $errors[] = "Can't find a User with the UserName '$UserName'. Please sign in.";
        }
        return $errors;
    }

  
    public function add()
    {
        self::execute(
            "INSERT INTO User(UserName,Password,FullName,Email) VALUES(:UserName,:Password,:FullName,:Email)",
            array("UserName" => $this->UserName, "Password" => $this->Password, "FullName" => $this->FullName, "Email" => $this->Email)
        );
        return $this;
    }

    public static function get_User_by_UserId($UserId)
    {
        $query = self::execute("SELECT * FROM User where UserId = :UserId", array("UserId" => $UserId));
        $data = $query->fetch();
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new User($data["UserName"], $data["Password"], $data["FullName"], $data["Email"], $data["UserId"] );
        }
    }
}
