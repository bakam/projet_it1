<?php

require_once 'User.php';
require_once 'framework/Model.php';
require_once 'framework/Tools.php';
require_once 'Vote.php';
require_once 'lib/parsedown-1.7.3/parsedown.php';

class Post extends Model {

    public $PostId;
    // public $AuthorId;
    public $Author;
    public $Title;
    public $Body;
    public $Timestamp;
    public $AcceptedAnswerId;
    public $ParentId;
    public $Vote;

    public function __construct($PostId = NULL, $Author, $Title, $Body, $Timestamp, $AcceptedAnswerId, $ParentId, $Vote = 0) {

        $this->PostId = $PostId;
        $this->Author = $Author;
        $this->Title = $Title;
        $this->Body = $Body;
        $this->Timestamp = $Timestamp;
        $this->AcceptedAnswerId = $AcceptedAnswerId;
        $this->Timestamp = $Timestamp;
        $this->ParentId = $ParentId;
        $this->Vote = $Vote;
    }

    public static function get_posts() {
        $query = self::execute("SELECT * FROM Post ", array());
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user, $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"], $countvote);
        }
        return $results;
    }

    public function get_Reponses_by_ParentId() {
        $query = self::execute("SELECT * FROM Post where ParentId = :ParentId", array("ParentId" => $this->PostId));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user, $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"], $countvote);
        }
        return $results;
    }

    private static function getBody($text) {
        $Parsedown = new Parsedown();
        $Parsedown->setsafeMode(true);
        $body = $Parsedown->text($text);
        return $body;
    }

    public function User() {
        return User::get_User_by_UserId($this->AuthorId)->FullName . " " . User::get_User_by_UserId($this->AuthorId)->UserName;
    }

    public function validate() {
        $errors = array();

        if (!(isset($this->Body) && is_string($this->Body) && strlen($this->Body) > 0)) {
            $errors[] = "Body must be filled";
        }

        return $errors;
    }

    public function write_post($post) {
        return $post->addPost();
    }

    public function update_post($post) {
        return $post->update();
    }

    public function addPost() {


        self::execute('INSERT INTO Post (AuthorId, Title, Body, Timestamp,AcceptedAnswerId,ParentId )'
                . ' VALUES (:authorid,:title,:body,:timestamp,:acceptedAnswerId,:parentId)', array(
            'authorid' => $this->Author->UserId,
            'title' => $this->Title,
            'body' => $this->Body,
            'timestamp' => $this->Timestamp,
            'acceptedAnswerId' => $this->AcceptedAnswerId,
            'parentId' => $this->ParentId,
        ));
        $post = self::getOnePostbyId(self::lastInsertId());
        $this->PostId = $post->PostId;
        $this->Timestamp = $post->Timestamp;


        return $this;
    }

    public function update() {

        self::execute('UPDATE  Post SET AuthorId = :authorid,Title = :title,Body = :body,Timestamp = :timestamp,AcceptedAnswerId= :acceptedAnswerId,ParentId= :parentId WHERE PostId=:id )', array(
            'authorid' => $this->Author->AuthorId,
            'title' => $this->Title,
            'body' => $this->body,
            'timestamp' => date('Y-m-d H:i:s'),
            'acceptedAnswerId' => $this->AcceptedAnswerId,
            'parentId' => $this->ParentId,
            'id' => $this->PostId
        ));
    }

    public static function acceptedAnswer($postid, $anwerid) {

        self::execute("UPDATE  Post SET Timestamp = :timestamp, AcceptedAnswerId = :acceptedAnswerId WHERE PostId = :id ", array(
            'timestamp' => date('Y-m-d H:i:s'),
            'acceptedAnswerId' => $anwerid,
            'id' => $postid
        ));
    }

    public static function updateAnswer($postid, $anwer) {

        self::execute('UPDATE  Post SET Timestamp = :timestamp, Body = :body WHERE PostId = :id ', array(
            'timestamp' => date('Y-m-d H:i:s'),
            'body' => $anwer,
            'id' => $postid
        ));
    }

    public static function delete($postid) {

        self::execute('DELETE Post, Vote FROM Post'
                . ' LEFT JOIN Vote ON  Vote.PostId = Post.PostId '
                . 'WHERE Post.PostId = :post_id', array('post_id' => $postid));
    }

    /**
     * 
     * @return \Post
     * affiche dans l'ordre chronologique inverse (la plus récente au début) du Timestamp  de
     * la question.
     */
    public static function newest() {
        $query = self::execute("SELECT * FROM post where ParentId is null ORDER BY Timestamp desc ", array());
       
        $data = $query->fetchall();
      
        $results = [];
        foreach ($data as $row) {
           
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user, $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"], $countvote);
        }
        return $results;
    }

    /**
     * 
     * @return \Post
     *  affiche dans l'ordre décroissant de score (le score pris en compte est le meilleur score de
      la question et de ses réponses).

     */
    public static function votes() {
        $query = self::execute("SELECT post.*, max_score
        FROM post, (
            SELECT parentid, max(score) max_score
            FROM (
                SELECT post.postid, ifnull(post.parentid, post.postid) parentid, ifnull(sum(vote.updown), 0) score
                FROM post LEFT JOIN vote ON vote.postid = post.postid
                GROUP BY post.postid
            ) AS tbl1
            GROUP by parentid
        ) AS q1
        WHERE post.postid = q1.parentid
        ORDER BY q1.max_score DESC, timestamp DESC  ", array());
        $data = $query->fetchall();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user, $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"], $countvote);
        }
        return $results;
    }

    /**
     * 
     * @return \Post
     *  : n'affiche que les questions pour lesquelles aucune réponse n'a été acceptée
     */
    public static function unanswered() {
        $query = self::execute("SELECT * FROM post WHERE AcceptedAnswerId is NULL and ParentId is null ORDER BY Timestamp desc", array());
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user , $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"],$countvote);
        }
        return $results;
    }

    /**
     * 
     * @param type $search
     * @param type $id
     * @return \Post
     *  permet de faire une recherche full text sur les données des posts (auteur compris), an de
     *  retrouver une question ou une réponse ; le résultat aché est la liste des questions qui satisfont
     *  au(x) critère(s) de recherche.
     */
    public static function search($search) {
        $query = self::execute("SELECT * FROM post, user WHERE Body = :body or Title = :title or UserName = :username ", array("body" => $search, "username" => $search, "title" => $search));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user , $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"],$countvote);
        }
        return $results;
    }

    public static function getPostbyId($PostId) {
        $query = self::execute("SELECT * FROM Post where PostId = :PostId", array("PostId" => $PostId));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
           $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user , $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"],$countvote);
        }
        return $results;
    }

    public static function getOnePostbyId($PostId) {
        $query = self::execute("SELECT * FROM post where PostId = :PostId", array("PostId" => $PostId));
        $data = $query->fetch();
        if ($query->rowCount() == 0) {
            return false;
        } else {
            $user = User::get_User_by_UserId($data["AuthorId"]);
            $countvote = Vote::getPostScore($data["PostId"]);
            return new Post($data["PostId"], $user, $data["Title"], $data["Body"], $data["Timestamp"], $data["AcceptedAnswerId"], $data["ParentId"],$countvote);
        }
    }

    /**
     * 
     * @return \Post
     * Affiche toutes les réponses
     */
    public static function getResponses($PostId) {
        $query = self::execute("SELECT * FROM post where ParentId = :id ", array("id" => $PostId));

        $data = $query->fetchAll();
        $results = [];
        if (!empty($data)) {
            foreach ($data as $row) {



                $user = User::get_User_by_UserId($row["AuthorId"]);
                $body = self::getBody($row["Body"]);
                $countvote = Vote::getPostScore($row["PostId"]);
                $results[] = new Post($row["PostId"], $user, $row["Title"], $body, $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"],$countvote);
            }
        }
        return $results;
    }

    /**
     * 
     * @return \Post
     * permet d'afficher une question ainsi que toutes les réponses correspondantes.
     */
    public function anwersByPostId($PostId) {
        $query = self::execute("SELECT * FROM Post where ParentId = : PostId ", array("PostId" => $PostId));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $user = User::get_User_by_UserId($row["AuthorId"]);
            $countvote = Vote::getPostScore($row["PostId"]);
            $results[] = new Post($row["PostId"], $user , $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"],$countvote);
        }
        return $results;
    }

     public static function check_db($db) {
        $query = self::execute("SHOW DATABASES LIKE '$db'", array());

       
        return $query->fetchColumn();
    }
    
   

    public function getScore() {
        $query = self::execute("SELECT SUM(UpDown) from vote where PostId = : PostId", array("PostId" => $this->PostId));
        return $query;
    }

    public function getVotes() {
        $query = self ::execute("SELECT * FROM vote   where PostId =:PostId  ", array("PostId" => $this->PostId));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $results[] = new Vote($row["UserId"], $row["PostId"], $row["UpDown"]);
        }
        return $results;
    }

    public function getParent() {
        $query = self::execute("SELECT * FROM Post where PostId = : ParentId ", array("PostId" => $this->ParentId));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $results[] = new Post($row["PostId"], $row["AuthorId"], $row["Title"], $row["Body"], $row["Timestamp"], $row["AcceptedAnswerId"], $row["ParentId"]);
        }
        return $results;
    }

}
