<?php

require_once 'User.php';
require_once 'framework/Model.php';

class Vote extends model {

    public $UserId;
    public $PostId;
    public $UpDown;

    public function User() {
        return User::get_User_by_UserId($this->UserId);
    }

    public function __construct($UserId, $PostId, $UpDown) {
        $this->UserId = $UserId;
        $this->PostId = $PostId;
        $this->UpDown = $UpDown;
    }

    public static function getPostScore($PostId) {
        $query = self::execute("SELECT SUM(UpDown) from vote where PostId = :PostId", array("PostId" => $PostId));
        return $query->fetchColumn();
    }

    public static function get_votes_by_PostId($PostId) {
        $query = self::execute("SELECT * FROM vote where PostId = :PostId", array("PostId" => $PostId));
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $results[] = new Vote($row["UserId"], $row["PostId"], $row["UpDown"]);
        }
        return $results;
    }

    public static function addVote($iduser, $idpost, $vote) {


        if (self::get_vote_by_id($idpost,$iduser))
            
            self::execute(" UPDATE  vote SET  UpDown = :updown  WHERE PostId = :idpost and UserId = :iduser", array(
                'iduser' => $iduser,
                'idpost' => $idpost,
                'updown' => $vote
            ));
        else
            self::execute('INSERT INTO vote (UserId, PostId, UpDown ) VALUES (:authorid,:postid,:updown)', array(
                'authorid' => $iduser,
                'postid' => $idpost,
                'updown' => $vote
            ));
        
    }

    public static function get_vote_by_id($idpost,$iduser) {
        $query = self::execute("SELECT * FROM Vote where PostId = :idpost and UserId = :iduser ", array("idpost" => $idpost,"iduser" => $iduser));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Vote($data["UserId"], $data["PostId"], $data["UpDown"]);
        }
    }

}
