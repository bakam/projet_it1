<?php

require_once 'model/User.php';
require_once 'model/Post.php';
require_once 'model/Data.php';
require_once 'framework/View.php';
require_once 'framework/Controller.php';

class ControllerUser extends Controller {

    //page d'accueil. 
    public function index() {
       
        if ($this->user_logged()) {
            $user = $this->get_user_or_redirect();

            (new View("ask"))->show(array("datas" => $datas, "user" => $user, "errors" => $errors));
        } else {
           
            (new View("loginuser"))->show(array("errors" => $errors));
            //  (new View("index"))->show(array("posts" => $posts, "user" => $user, "anwers" => $anwers, "users" => $users, "errors" => $errors));
        }
    }

   
   

    //gestion de la connexion d'un utilisateur
    public function login() {
        $username = '';
        $password = '';
        $errors = [];
        $user = NULL;
        if (isset($_POST['username']) && isset($_POST['password'])) { //note : pourraient contenir des chaînes vides
            
            $username = $_POST['username'];
            $password = $_POST['password'];
          
            $errors = User::validate_login($username, $password);
            
            if (empty($errors)) {
                 
                $this->log_user(User::get_User_by_UserName($username),$_SESSION['control'],$_SESSION['action']);
            }
            
        }
        (new View("loginuser"))->show(array("user" => $user,"username" => $username, "password" => $password, "errors" => $errors));
    }

    
     //gestion de l'inscription d'un utilisateur
    public function signup() {
       
        $username = '';
        $fullname = '';
        $email = '';
        $password = '';
        $password_confirm = '';
        $errors = [];
        $user = NULL;

        if (isset($_POST['username']) && isset($_POST['fullname']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {
            
            $username = trim($_POST['username']);
            $fullname = trim($_POST['fullname']);
            $email = trim($_POST['email']);
            $password = $_POST['password'];
            $password_confirm = $_POST['password_confirm'];

            $user = new User($username, Tools::my_hash($password),$fullname,$email);
            $errors = User::validate_unicity($username);
            $errors = array_merge($errors, $user->validate());
            $errors = array_merge($errors, User::validate_passwords($password, $password_confirm));

            if (count($errors) == 0) { 
                $user->update(); //sauve l'utilisateur
                $this->log_user($user);
            }
        }
        (new View("signupuser"))->show(array("user" => $user,"username" => $username, "password" => $password,"fullname" => $fullname, 
                                         "email" => $email, "password_confirm" => $password_confirm, "errors" => $errors));
    }
   

   
   

}
