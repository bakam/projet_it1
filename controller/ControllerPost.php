<?php

require_once 'model/User.php';
require_once 'model/Post.php';
require_once 'model/Data.php';
require_once 'framework/View.php';
require_once 'framework/Controller.php';

class ControllerPost extends Controller {

    //accueil du controlleur.
    //gère l'affichage des  posts
    public function index() {


        $this->start();
    }

    public function start() {
        $errors = [];

        $datas = Data::getData();
        $user = NULL;
        $body = '';

        if ($this->user_logged()) {
            $user = $this->get_user_or_redirect();

            (new View("index"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        } else {
            (new View("index"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        }
    }

    public function unanswered() {
        $errors = [];
        $datas = Data::unansweredData();
        $user = NULL;
        $body = '';

        if ($this->user_logged()) {
            $user = $this->get_user_or_redirect();

            (new View("unanswered"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        } else {
            (new View("unanswered"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        }
    }

    public function search() {
        $errors = [];
        $datas = [];
        $user = NULL;

        $body = '';
        if (isset($_POST['body']) && $_POST['body'] !== '') {

            $body = $_POST['body'];


            $datas = Data::searchData($body);

            if ($this->user_logged()) {
                $user = $this->get_user_or_redirect();
            }
            (new View("search"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        } else {

            $errors = "No text in search";
            if ($this->user_logged()) {
                $user = $this->get_user_or_redirect();
            }

            (new View("search"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        }
    }

    public function votes() {
        $errors = [];
        $datas = Data::votesData();
        $user = NULL;
        $body = '';

        if ($this->user_logged()) {
            $user = $this->get_user_or_redirect();

            (new View("votes"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        } else {
            (new View("votes"))->show(array("datas" => $datas, "body" => $body, "user" => $user, "errors" => $errors));
        }
    }

    public function writeQuestion() {

        $user = NULL;

        $body = '';
        $title = '';
        $errors = [];

        if ($this->user_logged()) {

            $user = $this->get_user_or_redirect();
            if (isset($_POST['body']) && isset($_POST['title'])) {
                $body = $_POST['body'];
                $title = $_POST['title'];
                $errors = $this->post($user, $body, $title);

                if (empty($errors)) {
                    $this->redirect("post", "index");
                } else {

                    (new View("ask"))->show(array("title" => $title, "body" => $body, "user" => $user, "errors" => $errors));
                }
            }
        } else { // pour la perte de connexion une fois le client connecté
            $_SESSION['action'] = $_GET["action"];
            $_SESSION['control'] = $_GET["controller"];
            $this->redirect("user", "login");
        }
    }

    private function post($user, $body, $title) {
        $errors = [];

        $post = new Post(NULL, $user, $title, $body, date('Y-m-d H:i:s'), NULL, NULL);
        $errors = $post->validate();
        if (empty($errors)) {
            $post->write_post($post);
        }

        return $errors;
    }

    private function anwer($user, $body, $id) {
        $errors = [];

        $post = new Post(NULL, $user, " ", $body, date('Y-m-d H:i:s'), NULL, $id);
        $errors = $post->validate();
        if (empty($errors)) {
            $post->write_post($post);
        }

        return $errors;
    }

    public function ask() {



        $title = '';
        $body = '';
        $errors = [];

        if ($this->user_logged()) {

            $user = $this->get_user_or_redirect();


            (new View("ask"))->show(array("title" => $title, "body" => $body, "user" => $user, "errors" => $errors));
        } else {

            $_SESSION['action'] = $_GET["action"];
            $_SESSION['control'] = $_GET["controller"];

            $this->redirect("user", "login");
        }
    }

    public function writeAnwer() {


        $errors = [];
        $user = NULL;
        $datas = [];
        $body = '';

        if ($this->user_logged()) {

            $user = $this->get_user_or_redirect();

            if (isset($_POST['body']) && isset($_POST['id'])) {

                $body = $_POST['body'];
                $id = $_POST['id'];

                $errors = $this->anwer($user, $body, $id);
                $datas = Data::getPostIdData($id);
            } else {

                $body = $_SESSION['body'];
                $id = $_SESSION['id'];
                print_r("flag=1 id: $id  body : $body");
                $errors = $this->anwer($user, $body, $id);
                $datas = Data::getPostIdData($id);
            }




            (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
        } else {


            $_SESSION['action'] = $_GET["action"];
            $_SESSION['control'] = $_GET["controller"];
            $_SESSION['id'] = $_POST['id'];
            $_SESSION['body'] = $_POST['body'];
            $this->redirect("user", "login");
        }
    }

    public function plus() {


        $user = NULL;

        $body = '';
        $title = '';
        $errors = [];

        if ($this->user_logged()) {

            $user = $this->get_user_or_redirect();

            if (isset($_GET["param1"]) && $_GET["param1"] !== "") {

                $id = $_GET["param1"];

                if (isset($_GET["param2"]) && $_GET["param2"] !== "") {

                    $id2 = $_GET["param2"];
                    Vote::addVote($user->UserId, $id2, 1);
                    $datas = Data::getPostIdData($id);
                } else {
                    Vote::addVote($user->UserId, $id, 1);
                    $datas = Data::getPostIdData($id);
                }
            } else {
                $id = $_SESSION['plus'];
                Vote::addVote($user->UserId, $id, 1);
                $datas = Data::getPostIdData($id);
            }

            (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
        } else { // pour la perte de connexion une fois le client connecté
            $_SESSION['action'] = $_GET["action"];
            $_SESSION['control'] = $_GET["controller"];
            $_SESSION['plus'] = $_GET["param1"];
            $this->redirect("user", "login");
        }
    }

    public function edit() {

        $user = NULL;

        $body = '';
        $title = '';
        $errors = [];
        $user = $this->get_user_or_redirect();


        $idpost = $_GET["param1"];
        $idanwer = $_GET["param2"];

        print_r(" $idpost  $idanwer");

        if (isset($_POST['body']) && isset($_POST['idpost']) && isset($_POST['idanwer'])) {

            $body = $_POST['body'];
            $idpost = $_POST['idpost'];
            $idanwer = $_POST['idanwer'];

            if (!empty($body) && $idanwer > 0) {
                Post::updateAnswer($idanwer, $body);
                $datas = Data::getPostIdData($idpost);
                (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
            } else if (!empty($body) && $idanwer < 0) {
                Post::updateAnswer($idpost, $body);
                $datas = Data::getPostIdData($idpost);
                (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
            } else {
                $errors = "text is empty";
                (new View("edit"))->show(array("idanwer" => $idanwer, "idpost" => $idpost, "body" => $body, "user" => $user, "errors" => $errors));
            }
        } else {

            if ($idanwer < 0) {
                (new View("edit"))->show(array("idanwer" => $idanwer, "idpost" => $idpost, "body" => $body, "user" => $user, "errors" => $errors));
            }

            (new View("edit"))->show(array("idanwer" => $idanwer, "idpost" => $idpost, "body" => $body, "user" => $user, "errors" => $errors));
        }
    }

    public function minus() {

        $user = NULL;

        $body = '';
        $title = '';
        $errors = [];

        if ($this->user_logged()) {

            $user = $this->get_user_or_redirect();

            if (isset($_GET["param1"]) && $_GET["param1"] !== "") {

                $id = $_GET["param1"];

                if (isset($_GET["param2"]) && $_GET["param2"] !== "") {

                    $id2 = $_GET["param2"];
                    Vote::addVote($user->UserId, $id2, -1);
                    $datas = Data::getPostIdData($id);
                } else {
                    Vote::addVote($user->UserId, $id, -1);
                    $datas = Data::getPostIdData($id);
                }
            } else {
                $id = $_SESSION['plus'];
                Vote::addVote($user->UserId, $id, -1);
                $datas = Data::getPostIdData($id);
            }

            (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
        } else { // pour la perte de connexion une fois le client connecté
            $_SESSION['action'] = $_GET["action"];
            $_SESSION['control'] = $_GET["controller"];
            $_SESSION['plus'] = $_GET["param1"];
            $this->redirect("user", "login");
        }
    }

    public function update() {

        $user = NULL;

        $body = '';

        $errors = [];


        $user = $this->get_user_or_redirect();
        $idpost = $_GET["param1"];
        $idanwer = $_GET["param2"];
        $flag = $_GET["param3"];


        if ($flag == 1) {
            Post::acceptedAnswer($idpost, $idanwer);
        } else {
            Post::acceptedAnswer($idpost, NULL);
        }

        $datas = Data::getPostIdData($idpost);


        (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
    }

    public function delete() {

        $user = NULL;

        $body = '';

        $errors = [];

        $user = $this->get_user_or_redirect();

        $idpost = $_GET["param1"];
        $idanwer = $_GET["param2"];
        $id = $_GET["param3"];

        if ($id == 0) {

            (new View("confirm"))->show(array("idanwer" => $idanwer, "idpost" => $idpost, "user" => $user));
        } else {

            if ($idanwer == -1) {
                Post::delete($idpost);
                $this->redirect("post", "index");
            } else if ($idanwer == -2) {
               
            } else {
                Post::delete($idanwer);
            }


            $datas = Data::getPostIdData($idpost);


            (new View("show"))->show(array("body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
        }
    }

    public function getPost() {


        $errors = [];
        $user = NULL;
        $body = '';
        $title = '';

        $datas = NULL;

        if ($this->user_logged()) {

            $user = $this->get_user_or_redirect();
        }
        if (isset($_GET["param1"]) && $_GET["param1"] !== "") {

            $datas = Data::getPostIdData($_GET["param1"]);
        } else {
            // pop no param
            $this->redirect("user", "login");
        }


        (new View("show"))->show(array("title" => $title, "body" => $body, "datas" => $datas, "user" => $user, "errors" => $errors));
    }

    public function confirm() {
        
    }

}
